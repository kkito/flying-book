<?php
require_once "lib/HamlPHP/HamlPHP.php";
require_once "lib/HamlPHP/Storage/FileStorage.php";
require_once "lib/RedBeanPHP/rb.php";
$parser = new HamlPHP(new FileStorage(dirname(__FILE__) . '/tmp/'));
$content = $parser->parseFile('tpl/index.haml');
echo $parser->evaluate($content);
