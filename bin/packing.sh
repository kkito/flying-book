#!/bin/bash

# usage: ./bin/packing.sh
# generate the app dir for deploy
timestamp=`date +"%Y%m%d-%H%M%S"`
FBDir=/tmp/flying-book-$timestamp
mkdir $FBDir
cp -rp lib $FBDir
cp -rp app/* $FBDir
mkdir $FBDir/tmp
echo $FBDir
