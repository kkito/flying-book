<?php
require_once "lib/HamlPHP/HamlPHP.php";
require_once "lib/HamlPHP/Storage/FileStorage.php";
require_once "lib/RedBeanPHP/rb.php";

define("DBFILE" , "E:/http/kkito/databases/flyingbook.sqlite3");

$parser = new HamlPHP(new FileStorage(dirname(__FILE__) . '/tmp/'));
$content = $parser->parseFile('tpl/index.haml');
echo $parser->evaluate($content);
R::setup('sqlite:'.DBFILE);
$post = R::dispense('testpost');
$post->text = 'Hello World';
$post->age = 12;

$id = R::store($post);       //Create or Update
$post = R::load('testpost',$id); //Retrieve
echo $post->text;
echo $post->age;

